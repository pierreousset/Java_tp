import java.util.Scanner;
import java.util.*;
public class EX8 {

    private static Scanner scanner = new Scanner(System.in);

    public static void create(int numberInt, Personnel p ){
        String nom = "";
        String prenom = "";
        int age = 0;
        String annee = "";
        int unite = 0;

        System.out.print("Nom : ");
        nom = scanner.next();
        System.out.print("Prénom : ");
        prenom = scanner.next();
        System.out.print("Age : ");
        try{
            age = scanner.nextInt();
        }catch (InputMismatchException e) {
            System.out.println("Invalide valeur!");
            scanner.nextInt();
        }
        System.out.print("Année d’entrée : ");
        annee = scanner.next();
        System.out.print("l’Unité : ");
        try{
            unite = scanner.nextInt();
        }catch (InputMismatchException e) {
            System.out.println("Invalide valeur!");
            scanner.nextInt();
        }


        if (numberInt == 1){
            System.out.println("Création Vendeur");
            p.ajouterEmploye(new Vendeur(prenom, nom, age, annee, unite));
        }
        else if (numberInt == 2){
            System.out.println("Création Technicien");
            p.ajouterEmploye(new Technicien(prenom, nom, age, annee, unite));
        }
        else if (numberInt == 3){
            System.out.println("Création Representant");
            p.ajouterEmploye(new Representant(prenom, nom, age, annee, unite));
        }
        else if (numberInt == 4){
            System.out.println("Création Manutention");
            p.ajouterEmploye(new Manutentionnaire(prenom, nom, age, annee, unite));
        }
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");

    }

    public static void main(String[] args) {
        Personnel p = new Personnel();
        boolean quit = false;

        while (!quit) {
            System.out.println("Bienvenue");
            System.out.println("Choisir un nombre : ");
            System.out.println("    0 : Quitter ");
            System.out.println("    1 : Lister les employers ");
            System.out.println("    2 : Rajouter des employers ");
            System.out.print("->");
            int number = scanner.nextInt();

            switch (number) {
                case 0:
                    System.out.println("Bye");
                    quit = true;
                    break;
                case 1:
                    p.afficherSalaires();
                    System.out.println(String.format(
                            "Le salaire moyen dans l'entreprise est de %.2f francs.",
                            p.salaireMoyen()
                    ));
                    break;
                case 2:
                    System.out.println("        Vous voulez rajouter : ");
                    System.out.println("            1 : Un vendeur");
                    System.out.println("            2 : Un technicien ");
                    System.out.println("            3 : Un Representant ");
                    System.out.println("            4 : Un Manutention ");
                    int numberInt = scanner.nextInt();
                    // Fonction de creation d'un employer
                    create(numberInt, p);
                    break;
                default:
                    System.out.println("Merci de choisir un bon numéro");
            }

            /*
            Personnel p = new Personnel();

            p.ajouterEmploye(new Vendeur("Pierre", "Business", 45, "1995", 30000));
            p.ajouterEmploye(new Representant("Léon", "Vendtout", 25, "2001", 20000));
            p.ajouterEmploye(new Technicien("Yves", "Bosseur", 28, "1998", 1000));
            p.ajouterEmploye(new Manutentionnaire("Jeanne", "Stocketout", 32, "1998", 45));
            p.ajouterEmploye(new TechnARisque("Jean", "Flippe", 28, "2000", 1000));
            p.ajouterEmploye(new ManutARisque("Al", "Abordage", 30, "2001", 45));

            p.afficherSalaires();
            System.out.println(String.format(
                    "Le salaire moyen dans l'entreprise est de %.2f francs.",
                    p.salaireMoyen()
            ));
            */
        }
    }
}
