import java.io.*;
import java.util.Scanner;

public class ex {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Saisir le chemin du répertoire a lister :");
        String chemin = scanner.nextLine();
        File file = new File(chemin);

        if (file.exists()) {
            System.out.println("Le chemin existe ");
            if (file.isDirectory()) {
                System.out.println("Le chemin va vers un dossier");
            } else {
                System.out.println("Le chemin va vers un fichier");
            }
        }

    }
}