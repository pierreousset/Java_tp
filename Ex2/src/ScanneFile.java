import java.io.*;
import java.util.*;

public class ScanneFile {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Saisir le chemain du répertoire a regarder : ");
        String dir = scanner.nextLine();
        System.out.println("Saisir une extention : ");
        String extention = scanner.nextLine();

        File file = new File(dir);
        File[] list = file.listFiles();

        FilenameFilter fileNameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(name.lastIndexOf('.')>0) {

                    int Index = name.lastIndexOf('.');

                    String str = name.substring(Index);

                    // match path name extension
                    if(str.equals(extention)) {
                        return true;
                    }
                }
                return false;
            }
        };

        for (int i = 0; i< list.length; i++){
            System.out.println(list[i]);
        }
    }
}
