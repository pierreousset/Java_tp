import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Scanner;

public class Download{

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        /*
            Je veux telecharger sur internet une image
            On regarde si le fichier existe
            si oui on change le nom pour qui n'écrase pas l'image déja existante
        */
        System.out.print("Url de l'image :");
        String urls = scanner.next();

        System.out.print("Telechagement de l'image ");

        // Recuperation de l'image sur le web
        URL url = new URL(urls);
        BufferedImage img = ImageIO.read(url);
        File file = new File("downloaded.jpg");

        if(file.exists()){
            File files = new File(".");
            File[] list = files.listFiles();
            File file2 = new File("downloaded" + list.length + ".jpg");
            ImageIO.write(img, "jpg", file2);
        }else {
            ImageIO.write(img, "jpg", file);
        }
    }
}
